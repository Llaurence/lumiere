msgid ""
msgstr ""
"Project-Id-Version: lumiere\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-06-15 16:33-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

# src/projects/chapters/views.rs:12
msgid "New chapter"
msgstr ""

# src/projects/chapters/views.rs:14
msgid "Back"
msgstr ""

# src/projects/chapters/views.rs:22
msgid "Reading"
msgstr ""

# src/projects/chapters/views.rs:26
msgid "Edit"
msgstr ""

# src/projects/chapters/views.rs:29
msgid "Delete"
msgstr ""

# src/projects/chapters/views.rs:35
msgid "Previous chapter"
msgstr ""

# src/projects/chapters/views.rs:42
msgid "Table of contents"
msgstr ""

# src/projects/chapters/views.rs:47
msgid "Next chapter"
msgstr ""

# src/projects/chapters/views.rs:65
msgid "Editing a chapter"
msgstr ""

# src/projects/chapters/views.rs:77
msgid "Name"
msgstr ""

# src/projects/chapters/views.rs:78
msgid "This field is optional."
msgstr ""

# src/projects/chapters/views.rs:84
msgid "Chapter number"
msgstr ""

# src/projects/chapters/views.rs:91
msgid "Volume number"
msgstr ""

# src/projects/chapters/views.rs:98
msgid "Publish chapter"
msgstr ""

# src/projects/chapters/views.rs:105
msgid "Set as private"
msgstr ""

# src/projects/chapters/views.rs:114
msgid "Set as published"
msgstr ""

# src/projects/chapters/views.rs:117
msgid "Translation language"
msgstr ""

# src/projects/chapters/views.rs:122
msgid "Translation"
msgstr ""

# src/projects/chapters/views.rs:128
msgid "Send"
msgstr ""

# src/projects/chapters/views.rs:137
msgid "Vol. {}, Ch. {}"
msgstr ""

# src/projects/chapters/views.rs:139
msgid "Ch. {}"
msgstr ""

# src/projects/views.rs:13
msgid "Projects"
msgstr ""

# src/projects/views.rs:15
msgid "Project list"
msgstr ""

# src/projects/views.rs:18
msgid "Searching for {}"
msgstr ""

# src/projects/views.rs:21
msgid "Search projects"
msgstr ""

# src/projects/views.rs:22
msgid "Search"
msgstr ""

# src/projects/views.rs:25
msgid "New project"
msgstr ""

# src/projects/views.rs:32
msgid "Authors"
msgstr ""

# src/projects/views.rs:75
msgid "Project cover"
msgstr ""

# src/projects/views.rs:84
msgid "Alt. names"
msgstr ""

# src/projects/views.rs:92
msgid "Language"
msgstr ""

# src/projects/views.rs:112
msgid "This project doesn't have a description in your language, showing it in English instead."
msgstr ""

# src/projects/views.rs:123
msgid "Chapter list"
msgstr ""

# src/projects/views.rs:132
msgid "Published"
msgstr ""

# src/projects/views.rs:155
msgid "No"
msgstr ""

# src/projects/views.rs:158
msgid "Publish"
msgstr ""

# src/projects/views.rs:172
msgid "Editing a project"
msgstr ""

# src/projects/views.rs:175
msgid "Go back to project"
msgstr ""

# src/projects/views.rs:189
msgid "Alternative names"
msgstr ""

# src/projects/views.rs:191
msgid "One name per line. Empty lines are ignored."
msgstr ""

# src/projects/views.rs:201
msgid "One author per line. Empty lines are ignored."
msgstr ""

# src/projects/views.rs:209
msgid "Original language"
msgstr ""

# src/projects/views.rs:214
msgid "English description"
msgstr ""

# src/projects/views.rs:217
msgid "You can use markdown in this field."
msgstr ""

# src/projects/views.rs:225
msgid "Description in your language"
msgstr ""

# src/users/views.rs:10
msgid "Login"
msgstr ""

# src/users/views.rs:23
msgid "Username"
msgstr ""

# src/users/views.rs:26
msgid "Password"
msgstr ""

# src/users/views.rs:36
msgid "Register"
msgstr ""

# src/users/views.rs:45
msgid "Username must be between 3 and 64 characters long"
msgstr ""

# src/users/views.rs:52
msgid "Password must be at least 8 characters long"
msgstr ""

# src/users/views.rs:55
msgid "Confirm password"
msgstr ""

# src/views.rs:30
msgid "A year ago"
msgid_plural "{0} years ago"
msgstr[0] ""

# src/views.rs:32
msgid "A year hence"
msgid_plural "{0} years hence"
msgstr[0] ""

# src/views.rs:36
msgid "A month ago"
msgid_plural "{0} months ago"
msgstr[0] ""

# src/views.rs:38
msgid "A month hence"
msgid_plural "{0} months hence"
msgstr[0] ""

# src/views.rs:42
msgid "A week ago"
msgid_plural "{0} weeks ago"
msgstr[0] ""

# src/views.rs:44
msgid "A week hence"
msgid_plural "{0} weeks hence"
msgstr[0] ""

# src/views.rs:48
msgid "A day ago"
msgid_plural "{0} days ago"
msgstr[0] ""

# src/views.rs:50
msgid "A day hence"
msgid_plural "{0} days hence"
msgstr[0] ""

# src/views.rs:54
msgid "An hour ago"
msgid_plural "{0} hours ago"
msgstr[0] ""

# src/views.rs:56
msgid "An hour hence"
msgid_plural "{0} hours hence"
msgstr[0] ""

# src/views.rs:60
msgid "A minute ago"
msgid_plural "{0} minutes ago"
msgstr[0] ""

# src/views.rs:62
msgid "A minute hence"
msgid_plural "{0} minutes hence"
msgstr[0] ""

# src/views.rs:65
msgid "Just now"
msgstr ""

# src/views.rs:120
msgid "Profile"
msgstr ""

# src/views.rs:123
msgid "Logout"
msgstr ""

# src/views.rs:140
msgid "German"
msgstr ""

# src/views.rs:143
msgid "English"
msgstr ""

# src/views.rs:146
msgid "French"
msgstr ""

# src/views.rs:149
msgid "Japanese"
msgstr ""

# src/views.rs:152
msgid "Korean"
msgstr ""
