SET TIME ZONE 'UTC';

CREATE TABLE IF NOT EXISTS projects
    ( id            SERIAL      PRIMARY KEY
    , name          TEXT        NOT NULL
    , alt_names     TEXT[]      NOT NULL
    , authors       TEXT[]      NOT NULL
    , language      TEXT        NOT NULL
    , description   HSTORE      NOT NULL
    , cover         BYTEA
    , published_at  TIMESTAMP
    , created_at    TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP
    , updated_at    TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP
    );

CREATE INDEX projects_name_ts ON projects USING GIN(TO_TSVECTOR('english', name));

CREATE TABLE IF NOT EXISTS users
    ( id            SERIAL      PRIMARY KEY
    , username      VARCHAR(64) UNIQUE NOT NULL
    , password      CHAR(60)    NOT NULL
    );

CREATE UNIQUE INDEX users_username ON users(username);

CREATE TABLE IF NOT EXISTS chapters
    ( id            SERIAL      PRIMARY KEY
    , name          TEXT
    , number        REAL        NOT NULL
    , volume        REAL
    , language      TEXT        NOT NULL
    , content       TEXT        NOT NULL DEFAULT ''
    , project_id    INTEGER     NOT NULL REFERENCES projects
    , published_at  TIMESTAMP
    , created_at    TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP
    , updated_at    TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP
    );

CREATE INDEX chapters_project_id ON chapters(project_id);
