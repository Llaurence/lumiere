DELETE FROM chapters;
DELETE FROM projects;
SELECT SETVAL('projects_id_seq', 1, FALSE),
       SETVAL('chapters_id_seq', 1, FALSE);

INSERT INTO projects (name, alt_names, authors, language, description)
VALUES
    ( 'High School DxD'
    , '{"ハイスクールD×D","Haisukūru Di Di"}'
    , '{"Ichiei Ishibumi","Miyama-Zero"}'
    , 'jp'
    , hstore(ARRAY['en', 'fr'], ARRAY['Lorem Ipsum','Baguette frommage'])
    ),
    ( 'Sekai Ichi no Imouto-sama'
    , '{세계 제일의 여동생님}'
    , '{"Kim Wolhui","nyanya"}'
    , 'kr'
    , hstore(ARRAY['en', 'fr'], ARRAY['Lorem Ipsum','Baguette frommage'])
    ),
    ( 'The Reunion With Twelve Fascinating Goddesses'
    , '{"異界神姫との再契約"}'
    , '{"Kota Nozomi","Merontomari"}'
    , 'jp'
    , hstore(ARRAY['en', 'fr'], ARRAY['Lorem Ipsum','Baguette frommage'])
    ),
    ( 'Famima!'
    , '{ふぁみまっ!}'
    , '{"Kube Kenji","Tsurusaki Takahiro"}'
    , 'jp'
    , hstore(ARRAY['en', 'fr'], ARRAY['Lorem Ipsum','Baguette frommage'])
    );

INSERT INTO chapters (name, number, volume, language, project_id)
VALUES
    (NULL, 0, 1, 'en', 1),
    ('I Quit Being A Human', 1, 1, 'en', 1),
    ('I Start As A Devil', 2, 1, 'en', 1),
    ('I Made A Friend', 3, 1, 'en', 1),
    (NULL, 0, 2, 'en', 1),
    (NULL, 0, 1, 'fr', 1),
    ('J''ai cessé d''être un Humain', 1, 1, 'fr', 1),
    ('Je débute en tant que Démon', 2, 1, 'fr', 1),
    ('Prologo', 0, 1, 'en', 4),
    ('Sabrina', 1, 1, 'en', 4),
    ('A Threatened Everyday Life!', 2, 1, 'en', 4),
    ('A Visitor From Italy', 3, 1, 'en', 4);
