use std::sync::Arc;

use actix::Addr;
use actix_web as web;
use actix_web::fs::StaticFiles;
use actix_web::middleware::identity::{CookieIdentityPolicy, IdentityService};
use actix_web::middleware::Logger;

use crate::db::DbExecutor;
use crate::i18n::{Internationalized, Translations};

pub type HttpRequest = web::HttpRequest<AppState>;
pub type Scope = web::Scope<AppState>;
pub type State = web::State<AppState>;

pub struct AppState {
    pub db: Addr<DbExecutor>,
    pub translations: Arc<Translations>,
}

impl Internationalized for AppState {
    fn get(&self) -> &Translations {
        use std::ops::Deref;
        self.translations.deref()
    }
}

pub fn new(db: Addr<DbExecutor>, translations: Arc<Translations>, key: &[u8])
           -> web::App<AppState>
{
    let static_files = StaticFiles::new("public").unwrap()
        .index_file("index.html");

    let auth = CookieIdentityPolicy::new(key)
        .name("lumiere")
        .path("/")
        .same_site(cookie::SameSite::Strict)
        .secure(false);
    let auth_service = IdentityService::new(auth);

    web::App::with_state(AppState { db, translations })
        .middleware(Logger::new("%a %Ts %s \"%r\""))
        .middleware(auth_service)
        .scope("/projects", crate::projects::routes)
        .scope("/users", crate::users::routes)
        .handler("/public", static_files)
}
