use actix::{Actor, SyncContext};
use r2d2_postgres::{PostgresConnectionManager, TlsMode};
use r2d2_postgres::postgres::params::{ConnectParams, IntoConnectParams};
use r2d2_postgres::r2d2::{Pool, PooledConnection};

const SQL_SET_TIME_ZONE: &str = "SET TIME ZONE 'UTC'";

pub type SqlConnection = PooledConnection<PostgresConnectionManager>;

#[derive(Clone)]
pub struct DbExecutor(pub Pool<PostgresConnectionManager>);

impl Actor for DbExecutor {
    type Context = SyncContext<Self>;
}

impl DbExecutor {
    pub fn new<P>(url: P) -> DbExecutor
        where P: IntoConnectParams
    {
        log::info!("Connecting to the database...");
        let params = url.into_connect_params().unwrap();
        let manager = PostgresConnectionManager::new(params.clone(),
                                                     TlsMode::None)
            .unwrap_or_else(|err| print_error(err, &params));

        let pool = Pool::new(manager)
            .unwrap_or_else(|err| print_error(err, &params));

        pool.get()
            .unwrap_or_else(|err| print_error(err, &params))
            .execute(SQL_SET_TIME_ZONE, &[])
            .unwrap_or_else(|err| {
                eprintln!("Could not set the database timezone: {}", err);
                std::process::exit(1);
            });
        log::info!("Connected to the database");
        DbExecutor(pool)
    }
}

fn print_error<T, E>(err: E, params: &ConnectParams) -> T
    where E: std::fmt::Display
{
    log::error!("Could not connect to PostgreSQL: {}", err);
    log::error!("Given connection parameters: {:?}", params);
    std::process::exit(1)
}
