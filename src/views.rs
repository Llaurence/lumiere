use gettext::Catalog;
use gettext_macros::i18n;
use maud::{DOCTYPE, html, Markup, Render};
use pulldown_cmark::{Event, Parser};
use pulldown_cmark::html::push_html;

use crate::i18n::I18n;
use crate::users::User;

pub struct Date<'a>(pub &'a I18n, pub chrono::NaiveDateTime);

impl<'a> Render for Date<'a> {
    fn render_to(&self, w: &mut String) {
        use chrono::{DateTime, Utc};

        let now = Utc::now();
        let then = DateTime::from_utc(self.1, Utc);
        let is_past = now > then;
        let elapsed = if is_past { now - then } else { then - now };

        let elapsed_minutes = elapsed.num_minutes();
        let elapsed_hours = elapsed.num_hours();
        let elapsed_days = elapsed.num_days();
        let elapsed_weeks = elapsed.num_weeks();
        let elapsed_months = elapsed_days / 30;
        let elapsed_years = elapsed_days / 365;

        let elapsed_str = if elapsed_years > 0 {
            if is_past {
                i18n!(self.0.catalog, "A year ago", "{0} years ago"; elapsed_years)
            } else {
                i18n!(self.0.catalog, "A year hence", "{0} years hence"; elapsed_years)
            }
        } else if elapsed_months > 0 {
            if is_past {
                i18n!(self.0.catalog, "A month ago", "{0} months ago"; elapsed_months)
            } else {
                i18n!(self.0.catalog, "A month hence", "{0} months hence"; elapsed_months)
            }
        } else if elapsed_weeks > 0 {
            if is_past {
                i18n!(self.0.catalog, "A week ago", "{0} weeks ago"; elapsed_weeks)
            } else {
                i18n!(self.0.catalog, "A week hence", "{0} weeks hence"; elapsed_weeks)
            }
        } else if elapsed_days > 0 {
            if is_past {
                i18n!(self.0.catalog, "A day ago", "{0} days ago"; elapsed_days)
            } else {
                i18n!(self.0.catalog, "A day hence", "{0} days hence"; elapsed_days)
            }
        } else if elapsed_hours > 0 {
            if is_past {
                i18n!(self.0.catalog, "An hour ago", "{0} hours ago"; elapsed_hours)
            } else {
                i18n!(self.0.catalog, "An hour hence", "{0} hours hence"; elapsed_hours)
            }
        } else if elapsed_minutes > 0 {
            if is_past {
                i18n!(self.0.catalog, "A minute ago", "{0} minutes ago"; elapsed_minutes)
            } else {
                i18n!(self.0.catalog, "A minute hence", "{0} minutes hence"; elapsed_minutes)
            }
        } else {
            i18n!(self.0.catalog, "Just now")
        };

        w.push_str("<span title=\"");
        w.push_str(&format!("{}", then.format("%Y/%m/%d %H:%M:%S UTC")));
        w.push_str("\">");
        w.push_str(&elapsed_str);
        w.push_str("</span>");
    }
}

pub struct Optional<T>(pub Option<T>);

impl<T: Render> Render for Optional<T> {
    fn render_to(&self, w: &mut String) {
        if let Some(ref v) = self.0 {
            v.render_to(w);
        }
    }
}

pub struct Markdown<'a>(pub &'a str);

impl<'a> Render for Markdown<'a> {
    fn render_to(&self, w: &mut String) {
        let parser = Parser::new(self.0)
            .map(|event| match event {
                Event::Html(html) |
                Event::InlineHtml(html) => Event::Text(html),
                event => event,
            });
        push_html(w, parser);
    }
}

pub fn head(i18n: &I18n, title: &str) -> Markup {
    html! {
        (DOCTYPE)
        html lang=(i18n.lang);
        meta charset="utf-8";
        meta name="viewport" content="width=device-width,initial-scale=1.0";
        link rel="stylesheet" type="text/css" href="/public/css/simple.css";
        link rel="stylesheet" type="text/css" href="/public/css/flags.css";
        title { (title) " - lumiere" }
    }
}

pub fn header(i18n: &I18n, user: Option<&User>) -> Markup {
    html! {
        header {
            a href="/projects/" { (i18n!(i18n.catalog, "Project list")) }
            " | "
            @if let Some(user) = user {
                "Logged in as " (user.username) " "
                a href="/users/me" {
                    button { (i18n!(i18n.catalog, "Profile")) }
                }
                form class="inline" method="POST" action="/users/logout" {
                    input type="submit" value=(i18n!(i18n.catalog, "Logout"));
                }
            } @else {
                a href="/users/login" {
                    button { (i18n!(i18n.catalog, "Login")) }
                }
                a href="/users/register" {
                    button { (i18n!(i18n.catalog, "Register")) }
                }
            }
        }
    }
}

pub fn lang_dropdown(c: &Catalog, selected: &str) -> Markup {
    html! {
        option value="de" selected?[selected == "de"] {
            (i18n!(c, "German"))
        }
        option value="en" selected?[selected == "en"] {
            (i18n!(c, "English"))
        }
        option value="fr" selected?[selected == "fr"] {
            (i18n!(c, "French"))
        }
        option value="jp" selected?[selected == "jp"] {
            (i18n!(c, "Japanese"))
        }
        option value="kr" selected?[selected == "kr"] {
            (i18n!(c, "Korean"))
        }
    }
}

pub fn lang_icon(lang: &str) -> Markup {
    html! {
        span class={"flag-icon flag-icon-"(lang)} {}
    }
}

pub fn br_list<T: Render>(xs: &[T]) -> Markup {
    html! {
        @let n = xs.len();
        @if n == 0 {
            // empty
        } @else {
            @for x in &xs[..n-1] {
                (x)
                br;
            }
            (xs[n-1])
        }
    }
}

pub fn comma_list<T: Render>(xs: &[T]) -> Markup {
    html! {
        @let n = xs.len();
        @if n == 0 {
            // empty
        } @else {
            @for x in &xs[..n-1] {
                (x) ", "
            }
            (xs[n-1])
        }
    }
}
