use std::{error, fmt};

use actix_web::{FromRequest, HttpRequest, ResponseError};
use gettext::Catalog;

const ACCEPT_LANG: &'static str = "Accept-Language";

/// A request guard to get the right translation catalog for the current request.
pub struct I18n {
    /// The catalog containing the translated messages, in the correct locale for this request.
    pub catalog: Catalog,
    /// The language of the current request.
    pub lang: &'static str,
}

pub type Translations = Vec<(&'static str, Catalog)>;

#[derive(Debug)]
pub struct MissingTranslationsError(String);

impl fmt::Display for MissingTranslationsError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Could not find translations for {}", self.0)
    }
}

impl error::Error for MissingTranslationsError {
    fn description(&self) -> &str {
        "Could not find translations"
    }
}

impl ResponseError for MissingTranslationsError {
    // this defaults to an empty InternalServerError response
}

pub trait Internationalized {
    fn get(&self) -> &Translations;
}

impl<S> FromRequest<S> for I18n
    where
        S: Internationalized,
{
    type Config = ();
    type Result = Result<Self, actix_web::Error>;

    fn from_request(req: &HttpRequest<S>, _: &Self::Config) -> Self::Result {
        let state = req.state();
        let langs = state.get();

        let lang = req
            .headers()
            .get(ACCEPT_LANG)
            .and_then(|v| v.to_str().ok())
            .unwrap_or("en")
            .split(",")
            .filter_map(|lang| {
                lang
                    // Get the locale, not the country code
                    .split(|c| c == '-' || c == ';')
                    .nth(0)
            })
            // Get the first requested locale we support
            .find(|lang| langs.iter().any(|l| l.0 == &lang.to_string()))
            .unwrap_or("en");

        match langs.iter().find(|l| l.0 == lang) {
            Some(translation) => Ok(I18n {
                catalog: translation.1.clone(),
                lang: translation.0,
            }),
            None => Err(MissingTranslationsError(lang.to_owned()).into()),
        }
    }
}
