use actix_web as web;
use actix_web::http::{Method, NormalizePath, StatusCode};
use futures::Future;
use maud::Markup;

use crate::app;
use crate::i18n::I18n;
use crate::users::User;

use self::models::*;

pub mod models;
mod views;

pub fn routes(scope: app::Scope) -> app::Scope {
    scope
        .resource("/", |r| {
            r.method(Method::POST).with_async(create);
        })
        .resource("/new", |r| {
            r.method(Method::GET).with(new);
        })
        .resource("/{id}", |r| {
            r.method(Method::GET).with_async(show);
            r.method(Method::POST).with_async(update);
        })
        .resource("/{id}/edit", |r| {
            r.method(Method::GET).with_async(edit);
        })
        .resource("/{id}/delete", |r| {
            r.method(Method::POST).with_async(delete);
        })
        .default_resource(|r| {
            // Add .method(Method::GET) to only redirect GET requests.
            r.h(NormalizePath::default());
        })
}

fn create((item, state): (web::Form<NewChapter>, app::State))
          -> impl Future<Item=web::HttpResponse, Error=web::Error>
{
    let data = item.into_inner();
    let project_id = data.project_id;
    state.db
        .send(CreateChapter { data })
        .from_err()
        .and_then(move |new_id| {
            let redirect = format!("/projects/{}/chapters/{}",
                                   project_id, new_id?);
            Ok(web::HttpResponse::build(StatusCode::SEE_OTHER)
                .header("Location", redirect)
                .finish())
        })
}

fn new((params, i18n, user): (web::Path<i32>, I18n, Option<User>))
       -> impl web::Responder {
    let project_id = params.into_inner();
    views::new(&i18n, user.as_ref(), project_id)
}

fn show((params, state, i18n, user):
        (web::Path<(i32, i32)>, app::State, I18n, Option<User>))
        -> impl Future<Item=Markup, Error=web::Error>
{
    let id = params.into_inner().1;
    state.db
        .send(FetchChapter { id })
        .from_err()
        .and_then(move |chapter| {
            Ok(views::show(&i18n, user.as_ref(), chapter?))
        })
}

fn update((params, item, state):
          (web::Path<(i32, i32)>, web::Form<NewChapter>, app::State))
          -> impl Future<Item=web::HttpResponse, Error=web::Error>
{
    let (project_id, id) = params.into_inner();
    let data = item.into_inner();
    state.db
        .send(UpdateChapter { id, data })
        .from_err()
        .and_then(move |res| {
            res?;
            let redirect = format!("/projects/{}/chapters/{}",
                                   project_id, id);
            Ok(web::HttpResponse::build(StatusCode::SEE_OTHER)
                .header("Location", redirect)
                .finish())
        })
}

fn edit((params, state, i18n, user):
        (web::Path<(i32, i32)>, app::State, I18n, Option<User>))
        -> impl Future<Item=Markup, Error=web::Error>
{
    let (_project_id, id) = params.into_inner();
    state.db
        .send(FetchChapter { id })
        .from_err()
        .and_then(move |chapter| {
            Ok(views::edit(&i18n, user.as_ref(), chapter?))
        })
}

fn delete((params, state): (web::Path<(i32, i32)>, app::State))
          -> impl Future<Item=web::HttpResponse, Error=web::Error>
{
    let (project_id, id) = params.into_inner();
    state.db
        .send(DeleteChapter { id })
        .from_err()
        .and_then(move |res| {
            res?;
            Ok(web::HttpResponse::build(StatusCode::SEE_OTHER)
                .header("Location", format!("/projects/{}", project_id))
                .finish())
        })
}
