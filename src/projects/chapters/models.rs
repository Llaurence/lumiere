use actix::{Handler, Message};
use actix_web as web;
use serde::Deserialize;

use crate::db::DbExecutor;

const SQL_FETCH_CHAPTER: &str = "
SELECT
    chapters.id,
    chapters.name,
    chapters.number,
    chapters.volume,
    chapters.language,
    chapters.content,
    chapters.published_at,
    projects.id,
    projects.name,
    projects.language
FROM chapters
JOIN projects ON project_id = projects.id
WHERE chapters.id = $1
";

const SQL_FETCH_NEXTPREV_CHAPTERS: &str = "
(   SELECT id, volume, number
    FROM chapters
    WHERE language = $3 AND (volume, number) > ($1, $2)
    ORDER BY volume ASC, number ASC
    LIMIT 1 )
UNION
(   SELECT id, volume, number
    FROM chapters
    WHERE language = $3 AND (volume, number) < ($1, $2)
    ORDER BY volume DESC, number DESC
    LIMIT 1 )
ORDER BY volume ASC, number ASC
";

const SQL_CREATE_CHAPTER: &str = "
INSERT INTO chapters (name, number, volume, language,
                      content, project_id, published_at)
VALUES ($1, $2, $3, $4, $5, $6, $7)
RETURNING id
";

const SQL_UPDATE_CHAPTER: &str = "
UPDATE chapters
SET name = $2,
    number = $3,
    volume = $4,
    language = $5,
    content = $6,
    published_at = $7,
    updated_at = CURRENT_TIMESTAMP
WHERE id = $1
";

const SQL_DELETE_CHAPTER: &str = "
DELETE FROM chapters
WHERE id = $1
";

pub struct ChapterInfo {
    pub id: i32,
    pub name: Option<String>,
    pub number: f32,
    pub volume: Option<f32>,
    pub language: String,
    pub published_at: Option<chrono::NaiveDateTime>,
    pub project_id: i32,
}

#[derive(Default)]
pub struct Chapter {
    pub id: i32,
    pub name: Option<String>,
    pub number: f32,
    pub volume: Option<f32>,
    pub language: String,
    pub content: String,
    pub published_at: Option<chrono::NaiveDateTime>,
    pub project_id: i32,
    pub project_name: String,
    pub project_language: String,
    pub prev_chapter_id: i32,
    pub next_chapter_id: i32,
}

#[derive(Deserialize)]
pub struct NewChapter {
    pub name: Option<String>,
    pub number: f32,
    pub volume: Option<f32>,
    pub publish: i8,
    pub language: String,
    pub content: String,
    pub project_id: i32,
}

impl NewChapter {
    pub fn name(&self) -> Option<&str> {
        self.name.as_ref()
            .map(|name| name.trim())
            .filter(|&name| !name.is_empty())
    }

    pub fn language(&self) -> &str {
        self.language.trim()
    }

    pub fn content(&self) -> &str {
        self.content.trim()
    }

    pub fn published_at(&self) -> Option<chrono::NaiveDateTime> {
        if self.publish == 1 {
            Some(chrono::Utc::now().naive_utc())
        } else {
            None
        }
    }
}

pub struct FetchChapter {
    pub id: i32,
}

impl Message for FetchChapter {
    type Result = Result<Chapter, web::Error>;
}

impl Handler<FetchChapter> for DbExecutor {
    type Result = <FetchChapter as Message>::Result;

    fn handle(&mut self, msg: FetchChapter, _: &mut Self::Context) -> Self::Result {
        let conn = &self.0.get().unwrap();

        let mut chapter = conn.query(SQL_FETCH_CHAPTER, &[&msg.id])
            .map_err(web::error::ErrorInternalServerError)?
            .iter()
            .map(|row| Chapter {
                id: row.get(0),
                name: row.get(1),
                number: row.get(2),
                volume: row.get(3),
                language: row.get(4),
                content: row.get(5),
                published_at: row.get(6),
                project_id: row.get(7),
                project_name: row.get(8),
                project_language: row.get(9),
                prev_chapter_id: 0,
                next_chapter_id: 0,
            })
            .next()
            .ok_or_else(|| {
                web::error::ErrorInternalServerError("Chapter not found")
            })?;

        let volume = chapter.volume.unwrap_or(0.0);
        let np_chapters = conn.query(SQL_FETCH_NEXTPREV_CHAPTERS,
                                     &[&volume, &chapter.number,
                                         &chapter.language])
            .map_err(web::error::ErrorInternalServerError)?;
        let mut np_chapters = np_chapters.iter()
            .map(|row| {
                (row.get(0),
                 row.get::<_, Option<f32>>(1).unwrap_or(0.0),
                 row.get::<_, f32>(2))
            });
        let prev_chapter = np_chapters.next();
        let next_chapter = np_chapters.next();
        if let Some((id, _, _)) = next_chapter {
            chapter.next_chapter_id = id;
        }
        if let Some((id, v, n)) = prev_chapter {
            if v < chapter.volume.unwrap_or(0.0) || n < chapter.number {
                chapter.prev_chapter_id = id;
            } else {
                chapter.next_chapter_id = id;
            }
        }

        Ok(chapter)
    }
}

pub struct CreateChapter {
    pub data: NewChapter,
}

impl Message for CreateChapter {
    type Result = Result<i32, web::Error>;
}

impl Handler<CreateChapter> for DbExecutor {
    type Result = <CreateChapter as Message>::Result;

    fn handle(&mut self, msg: CreateChapter, _: &mut Self::Context) -> Self::Result {
        let conn = &self.0.get().unwrap();
        let c = msg.data;
        let new_id = conn.query(SQL_CREATE_CHAPTER,
                                &[&c.name(), &c.number, &c.volume, &c.language(),
                                    &c.content(), &c.project_id, &c.published_at()])
            .map_err(web::error::ErrorInternalServerError)?
            .get(0)
            .get(0);
        Ok(new_id)
    }
}

pub struct UpdateChapter {
    pub id: i32,
    pub data: NewChapter,
}

impl Message for UpdateChapter {
    type Result = Result<(), web::Error>;
}

impl Handler<UpdateChapter> for DbExecutor {
    type Result = <UpdateChapter as Message>::Result;

    fn handle(&mut self, msg: UpdateChapter, _: &mut Self::Context) -> Self::Result {
        let conn = &self.0.get().unwrap();
        let c = msg.data;
        conn.execute(SQL_UPDATE_CHAPTER,
                     &[&msg.id, &c.name(), &c.number, &c.volume,
                         &c.language(), &c.content(), &c.published_at()])
            .map_err(web::error::ErrorInternalServerError)?;
        Ok(())
    }
}

pub struct DeleteChapter {
    pub id: i32,
}

impl Message for DeleteChapter {
    type Result = Result<(), web::Error>;
}

impl Handler<DeleteChapter> for DbExecutor {
    type Result = <DeleteChapter as Message>::Result;

    fn handle(&mut self, msg: DeleteChapter, _: &mut Self::Context) -> Self::Result {
        let conn = &self.0.get().unwrap();
        conn.execute(SQL_DELETE_CHAPTER, &[&msg.id])
            .map_err(web::error::ErrorInternalServerError)?;
        Ok(())
    }
}
