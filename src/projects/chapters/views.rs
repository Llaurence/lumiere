use gettext_macros::i18n;
use maud::{html, Markup};

use crate::i18n::I18n;
use crate::users::User;
use crate::views::{head, header, lang_dropdown, Markdown, Optional};

use super::models::Chapter;

pub fn new(i18n: &I18n, user: Option<&User>, project_id: i32) -> Markup {
    html! {
        (head(i18n, &i18n!(i18n.catalog, "New chapter")))
        (header(&i18n, user))
        a href={"/projects/"(project_id)} { (i18n!(i18n.catalog, "Back")) }
        h1 { (i18n!(i18n.catalog, "New chapter")) }
        (form(i18n, Chapter { project_id, ..Chapter::default() }))
    }
}

pub fn show(i18n: &I18n, user: Option<&User>, chapter: Chapter) -> Markup {
    html! {
        (head(i18n, &i18n!(i18n.catalog, "Reading")))
        (header(&i18n, user))
        div class="chapter actions" {
            a href={"/projects/"(chapter.project_id)"/chapters/"(chapter.id)"/edit"} {
                button { (i18n!(i18n.catalog, "Edit")) }
            }
            form class="inline" method="POST" action={"/projects/"(chapter.project_id)"/chapters/"(chapter.id)"/delete"} {
                input type="submit" value={(i18n!(i18n.catalog, "Delete"))};
            }
        }
        nav class="chapter" {
            @if chapter.prev_chapter_id > 0 {
                a href={"/projects/"(chapter.project_id)"/chapters/"(chapter.prev_chapter_id)} {
                    (i18n!(i18n.catalog, "Previous chapter"))
                }
            } @else {
                (i18n!(i18n.catalog, "Previous chapter"))
            }
            " | "
            a href={"/projects/"(chapter.project_id)} {
                (i18n!(i18n.catalog, "Table of contents"))
            }
            " | "
            @if chapter.next_chapter_id > 0 {
                a href={"/projects/"(chapter.project_id)"/chapters/"(chapter.next_chapter_id)} {
                    (i18n!(i18n.catalog, "Next chapter"))
                }
            } @else {
                (i18n!(i18n.catalog, "Next chapter"))
            }
        }
        h1 { (full_number(i18n, &chapter)) }
        @if let Some(ref name) = chapter.name {
            h2 class="subtitle" { (name) }
        }
        div class="content" {
            (Markdown(&chapter.content))
        }
    }
}

pub fn edit(i18n: &I18n, user: Option<&User>, chapter: Chapter) -> Markup {
    html! {
        (head(i18n, &i18n!(i18n.catalog, "Editing a chapter")))
        (header(&i18n, user))
        a href={"/projects/"(chapter.project_id)} { (i18n!(i18n.catalog, "Back")) }
        h1 { (i18n!(i18n.catalog, "Editing a chapter")) }
        h2 class="subtitle" { (full_number(i18n, &chapter)) }
        (form(i18n, chapter))
    }
}

fn form(i18n: &I18n, chapter: Chapter) -> Markup {
    html! {
        form class="details" method="POST" action={ "/projects/" (chapter.project_id) "/chapters/" {@if chapter.id > 0 { (chapter.id) }} } {
            label for="name" { (i18n!(i18n.catalog, "Name")) }
            em { (i18n!(i18n.catalog, "This field is optional.")) }
            input type="text"
                  name="name"
                  id="name"
                  value=(Optional(chapter.name));

            label for="number" { (i18n!(i18n.catalog, "Chapter number")) }
            input type="text"
                  name="number"
                  id="number"
                  value=(chapter.number)
                  required?;

            label for="volume" { (i18n!(i18n.catalog, "Volume number")) }
            em { (i18n!(i18n.catalog, "This field is optional.")) }
            input type="text"
                  name="volume"
                  id="volume"
                  value=(Optional(chapter.volume));

            label id="publish" { (i18n!(i18n.catalog, "Publish chapter")) }
            input type="radio"
                  name="publish"
                  id="do-not-publish"
                  value="0"
                  checked?[chapter.published_at.is_none()];
            label for="do-not-publish" {
                (i18n!(i18n.catalog, "Set as private"))
            }
            br;
            input type="radio"
                  name="publish"
                  id="publish-now"
                  value="1"
                  checked?[chapter.published_at.is_some()];
            label for="publish-now" {
                (i18n!(i18n.catalog, "Set as published"))
            }

            label for="language" { (i18n!(i18n.catalog, "Translation language")) }
            select name="language" id="content" required? {
                (lang_dropdown(&i18n.catalog, &chapter.language))
            }

            label for="content" { (i18n!(i18n.catalog, "Translation")) }
            textarea name="content" id="content" {
                (chapter.content)
            }

            input type="hidden" name="project_id" value=(chapter.project_id);
            input type="submit" value=(i18n!(i18n.catalog, "Send"));
        }
    }
}

fn full_number(i18n: &I18n, chapter: &Chapter) -> Markup {
    html! {
        (chapter.project_name) ", "
        @if let Some(ref volume) = chapter.volume {
            (i18n!(i18n.catalog, "Vol. {}, Ch. {}"; volume, &chapter.number))
        } @else {
            (i18n!(i18n.catalog, "Ch. {}"; &chapter.number))
        }
    }
}
