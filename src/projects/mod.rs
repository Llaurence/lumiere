use actix_web as web;
use actix_web::http::{header, Method, NormalizePath};
use futures::Future;
use maud::Markup;

use crate::app;
use crate::i18n::I18n;
use crate::users::User;

use self::models::*;

mod chapters;
mod models;
mod views;

pub fn routes(scope: app::Scope) -> app::Scope {
    scope
        .resource("/", |r| {
            r.method(Method::GET).with_async(index);
            r.method(Method::POST).with_async(create);
        })
        .resource("/new", |r| {
            r.method(Method::GET).with(new);
        })
        .resource("/{id}", |r| {
            r.method(Method::GET).with_async(show);
            r.method(Method::POST).with_async(update);
        })
        .resource("/{id}/cover.jpg", |r| {
            r.method(Method::GET).with_async(cover);
        })
        .resource("/{id}/edit", |r| {
            r.method(Method::GET).with_async(edit);
        })
        .resource("/{id}/delete", |r| {
            r.method(Method::POST).with_async(delete);
        })
        .nested("/{id}/chapters", chapters::routes)
        .default_resource(|r| {
            // Add .method(Method::GET) to only redirect GET requests.
            r.h(NormalizePath::default());
        })
}

fn index((search, state, i18n, user):
         (web::Query<ProjectSearch>, app::State, I18n, Option<User>))
         -> impl Future<Item=Markup, Error=web::Error>
{
    let language = i18n.lang;
    let search = search.into_inner().search;
    state.db
        .send(FetchProjects { language, search: search.clone() })
        .from_err()
        .and_then(move |projects| {
            Ok(views::index(&i18n, user.as_ref(), projects?, search))
        })
}

fn create((item, state, i18n):
          (web::Form<NewProject>, app::State, I18n))
          -> impl Future<Item=web::HttpResponse, Error=web::Error>
{
    let data = item.into_inner();
    let language = i18n.lang;
    state.db
        .send(CreateProject { data, language })
        .from_err()
        .and_then(|new_id| {
            Ok(web::HttpResponse::SeeOther()
                .header(header::LOCATION, format!("/projects/{}", new_id?))
                .finish())
        })
}

fn new((i18n, user): (I18n, Option<User>)) -> impl web::Responder {
    views::new(&i18n, user.as_ref())
}

fn show((params, state, i18n, user):
        (web::Path<i32>, app::State, I18n, Option<User>))
        -> impl Future<Item=Markup, Error=web::Error>
{
    let id = params.into_inner();
    let language = i18n.lang;
    state.db
        .send(FetchProjectWithChapters { id, language })
        .from_err()
        .and_then(move |project| {
            Ok(views::show(&i18n, user.as_ref(), project?))
        })
}

fn update((params, item, state, i18n):
          (web::Path<i32>, web::Form<NewProject>, app::State, I18n))
          -> impl Future<Item=web::HttpResponse, Error=web::Error>
{
    let id = params.into_inner();
    let data = item.into_inner();
    let language = i18n.lang;
    state.db
        .send(UpdateProject { id, data, language })
        .from_err()
        .and_then(move |res| {
            res?;
            Ok(web::HttpResponse::SeeOther()
                .header(header::LOCATION, format!("/projects/{}", id))
                .finish())
        })
}

fn cover((params, state): (web::Path<i32>, app::State))
         -> impl Future<Item=web::HttpResponse, Error=web::Error>
{
    let id = params.into_inner();
    state.db
        .send(FetchCover { id })
        .from_err()
        .and_then(move |res| {
            if let Some(data) = res? {
                Ok(web::HttpResponse::Ok()
                    .header(header::CACHE_CONTROL, "public, max-age=86400")
                    .header(header::CONTENT_TYPE, "image/jpeg")
                    .body(data))
            } else {
                Ok(web::HttpResponse::NotFound().finish())
            }
        })
}

fn edit((params, state, i18n, user):
        (web::Path<i32>, app::State, I18n, Option<User>))
        -> impl Future<Item=Markup, Error=web::Error>
{
    let id = params.into_inner();
    let language = i18n.lang;
    state.db
        .send(FetchProject { id, language })
        .from_err()
        .and_then(move |project| {
            Ok(views::edit(&i18n, user.as_ref(), project?))
        })
}

fn delete((params, state): (web::Path<i32>, app::State))
          -> impl Future<Item=web::HttpResponse, Error=web::Error>
{
    let id = params.into_inner();
    state.db
        .send(DeleteProject { id })
        .from_err()
        .and_then(|res| {
            res?;
            Ok(web::HttpResponse::SeeOther()
                .header(header::LOCATION, "/projects/")
                .finish())
        })
}
