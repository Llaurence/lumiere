use actix::{Handler, Message};
use actix_web as web;
use r2d2_postgres::postgres::rows::Row;
use serde::Deserialize;

use crate::db::{DbExecutor, SqlConnection};

use super::chapters::models::ChapterInfo;

const SQL_FETCH_PROJECTS: &str = "
SELECT
    id,
    name,
    alt_names,
    authors,
    language,
    description -> $1 AS description,
    description -> 'en' AS description_en,
    cover IS NOT NULL AS has_cover
FROM projects
";

const SQL_FETCH_PROJECTS_SEARCH: &str = "
SELECT
    id,
    name,
    alt_names,
    authors,
    language,
    description -> $1 AS description,
    description -> 'en' AS description_en,
    cover IS NOT NULL AS has_cover,
    TS_RANK(name_vector, search_query) AS rank
FROM
    projects,
    WEBSEARCH_TO_TSQUERY('english', $2) search_query,
    TO_TSVECTOR(name) name_vector
WHERE name_vector @@ search_query
ORDER BY rank DESC
";

const SQL_FETCH_PROJECT: &str = "
SELECT
    id,
    name,
    alt_names,
    authors,
    language,
    description -> $2 AS description,
    description -> 'en' AS description_en,
    cover IS NOT NULL AS has_cover
FROM projects
WHERE id = $1
";

const SQL_FETCH_PROJECT_CHAPTERS: &str = "
SELECT
    id,
    name,
    number,
    volume,
    language,
    published_at
FROM chapters
WHERE project_id = $1
ORDER BY volume DESC, number DESC, language DESC
";

const SQL_FETCH_COVER: &str = "
SELECT cover FROM projects WHERE id = $1
";

const SQL_CREATE_PROJECT: &str = "
INSERT INTO projects (name, alt_names, authors, language, description)
VALUES ($1, $2, $3, $4, hstore('en', $5))
RETURNING id
";

const SQL_CREATE_PROJECT_WITH_FOREIGN_DESCRIPTION: &str = "
INSERT INTO projects (name, alt_names, authors, language, description)
VALUES ($1, $2, $3, $4, hstore(ARRAY['en',$6], ARRAY[$5,$7]))
RETURNING id
";

const SQL_UPDATE_PROJECT: &str = "
UPDATE projects
SET name = $2,
    alt_names = $3,
    authors = $4,
    language = $5,
    description = description || hstore('en', $6),
    updated_at = CURRENT_TIMESTAMP
WHERE id = $1
";

const SQL_UPDATE_PROJECT_WITH_FOREIGN_DESCRIPTION: &str = "
UPDATE projects
SET name = $2,
    alt_names = $3,
    authors = $4,
    language = $5,
    description = description || hstore(ARRAY['en',$7], ARRAY[$6,$8]),
    updated_at = CURRENT_TIMESTAMP
WHERE id = $1
";

const SQL_DELETE_PROJECT: &str = "
DELETE FROM projects
WHERE id = $1
";

#[derive(Default)]
pub struct Project {
    pub id: i32,
    pub name: String,
    pub alt_names: Vec<String>,
    pub authors: Vec<String>,
    pub language: String,
    pub description: Option<String>,
    pub description_en: String,
    pub has_cover: bool,
}

impl Project {
    pub fn all(conn: &SqlConnection, lang: &str) -> Result<Vec<Project>, web::Error> {
        Ok(conn.query(SQL_FETCH_PROJECTS, &[&lang])
            .map_err(web::error::ErrorInternalServerError)?
            .iter()
            .map(Project::from_row)
            .collect())
    }

    pub fn find(conn: &SqlConnection, id: i32, lang: &str) -> Result<Project, web::Error> {
        conn.query(SQL_FETCH_PROJECT, &[&id, &lang])
            .map_err(web::error::ErrorInternalServerError)?
            .iter()
            .map(Project::from_row)
            .next()
            .ok_or_else(|| {
                web::error::ErrorInternalServerError("Project not found")
            })
    }

    pub fn search(conn: &SqlConnection, search: String, lang: &str) -> Result<Vec<Project>, web::Error> {
        Ok(conn.query(SQL_FETCH_PROJECTS_SEARCH, &[&lang, &search])
            .map_err(web::error::ErrorInternalServerError)?
            .iter()
            .map(Project::from_row)
            .collect())
    }

    pub fn has_locale_description(&self) -> bool {
        self.description.is_some()
    }

    pub fn description(&self) -> &str {
        self.description.as_ref().unwrap_or(&self.description_en)
    }

    fn from_row(row: Row) -> Project {
        Project {
            id: row.get(0),
            name: row.get(1),
            alt_names: row.get(2),
            authors: row.get(3),
            language: row.get(4),
            description: row.get(5),
            description_en: row.get(6),
            has_cover: row.get(7),
        }
    }
}

pub struct ProjectWithChapters {
    pub info: Project,
    pub chapters: Vec<ChapterInfo>,
}

#[derive(Deserialize)]
pub struct NewProject {
    pub name: String,
    pub alt_names: Option<String>,
    pub authors: String,
    pub language: String,
    pub description_en: String,
    pub description: Option<String>,
}

impl NewProject {
    pub fn name(&self) -> &str {
        self.name.trim()
    }

    pub fn alt_names(&self) -> Vec<&str> {
        self.alt_names.as_ref().map_or_else(Vec::new, parse_array)
    }

    pub fn authors(&self) -> Vec<&str> {
        parse_array(&self.authors)
    }

    pub fn language(&self) -> &str {
        self.language.trim()
    }

    pub fn description_en(&self) -> &str {
        self.description_en.trim()
    }

    pub fn description(&self) -> Option<Option<&str>> {
        self.description.as_ref()
            .map(|desc| desc.trim())
            .map(|desc| if desc.len() > 0 { Some(desc) } else { None })
    }
}

#[derive(Deserialize)]
pub struct ProjectSearch {
    pub search: Option<String>,
}

pub struct FetchProjects {
    pub language: &'static str,
    pub search: Option<String>,
}

impl Message for FetchProjects {
    type Result = <DbExecutor as Handler<FetchProjects>>::Result;
}

impl Handler<FetchProjects> for DbExecutor {
    type Result = Result<Vec<Project>, web::Error>;

    fn handle(&mut self, msg: FetchProjects, _: &mut Self::Context) -> Self::Result {
        let conn = &self.0.get().unwrap();
        if let Some(search) = msg.search {
            Project::search(conn, search, msg.language)
        } else {
            Project::all(conn, msg.language)
        }
    }
}

pub struct FetchProject {
    pub id: i32,
    pub language: &'static str,
}

impl Message for FetchProject {
    type Result = Result<Project, web::Error>;
}

impl Handler<FetchProject> for DbExecutor {
    type Result = <FetchProject as Message>::Result;

    fn handle(&mut self, msg: FetchProject, _: &mut Self::Context) -> Self::Result {
        let conn = &self.0.get().unwrap();
        Project::find(conn, msg.id, msg.language)
    }
}

pub struct FetchProjectWithChapters {
    pub id: i32,
    pub language: &'static str,
}

impl Message for FetchProjectWithChapters {
    type Result = Result<ProjectWithChapters, web::Error>;
}

impl Handler<FetchProjectWithChapters> for DbExecutor {
    type Result = <FetchProjectWithChapters as Message>::Result;

    fn handle(&mut self, msg: FetchProjectWithChapters, _: &mut Self::Context) -> Self::Result {
        let conn = &self.0.get().unwrap();

        let info = Project::find(conn, msg.id, msg.language)?;

        let chapters = conn.query(SQL_FETCH_PROJECT_CHAPTERS, &[&msg.id])
            .map_err(web::error::ErrorInternalServerError)?
            .iter()
            .map(|row| ChapterInfo {
                id: row.get(0),
                name: row.get(1),
                number: row.get(2),
                volume: row.get(3),
                language: row.get(4),
                published_at: row.get(5),
                project_id: msg.id,
            })
            .collect();

        Ok(ProjectWithChapters { info, chapters })
    }
}

pub struct FetchCover {
    pub id: i32,
}

impl Message for FetchCover {
    type Result = Result<Option<Vec<u8>>, web::Error>;
}

impl Handler<FetchCover> for DbExecutor {
    type Result = <FetchCover as Message>::Result;

    fn handle(&mut self, msg: FetchCover, _: &mut Self::Context) -> Self::Result {
        let conn = &self.0.get().unwrap();
        let cover = conn.query(SQL_FETCH_COVER, &[&msg.id])
            .map_err(web::error::ErrorInternalServerError)?
            .iter()
            .next()
            .ok_or_else(|| {
                web::error::ErrorInternalServerError("Project not found")
            })?
            .get(0);
        Ok(cover)
    }
}

pub struct CreateProject {
    pub data: NewProject,
    pub language: &'static str,
}

impl Message for CreateProject {
    type Result = Result<i32, web::Error>;
}

impl Handler<CreateProject> for DbExecutor {
    type Result = <CreateProject as Message>::Result;

    fn handle(&mut self, msg: CreateProject, _: &mut Self::Context) -> Self::Result {
        let conn = &self.0.get().unwrap();
        let p = msg.data;
        let query = if let Some(description) = p.description() {
            conn.query(SQL_CREATE_PROJECT_WITH_FOREIGN_DESCRIPTION,
                       &[&p.name(), &p.alt_names(), &p.authors(), &p.language(),
                           &p.description_en(), &msg.language, &description])
        } else {
            conn.query(SQL_CREATE_PROJECT,
                       &[&p.name(), &p.alt_names(), &p.authors(),
                           &p.language(), &p.description_en()])
        };
        let id = query
            .map_err(web::error::ErrorInternalServerError)?
            .get(0)
            .get(0);
        Ok(id)
    }
}

pub struct UpdateProject {
    pub id: i32,
    pub data: NewProject,
    pub language: &'static str,
}

impl Message for UpdateProject {
    type Result = Result<(), web::Error>;
}

impl Handler<UpdateProject> for DbExecutor {
    type Result = <UpdateProject as Message>::Result;

    fn handle(&mut self, msg: UpdateProject, _: &mut Self::Context) -> Self::Result {
        let conn = &self.0.get().unwrap();
        let p = msg.data;
        let query = if let Some(description) = p.description() {
            conn.execute(SQL_UPDATE_PROJECT_WITH_FOREIGN_DESCRIPTION,
                         &[&msg.id, &p.name(), &p.alt_names(), &p.authors(),
                             &p.language(), &p.description_en(),
                             &msg.language, &description])
        } else {
            conn.execute(SQL_UPDATE_PROJECT,
                         &[&msg.id, &p.name(), &p.alt_names(), &p.authors(),
                             &p.language(), &p.description_en()])
        };
        query.map_err(web::error::ErrorInternalServerError)?;
        Ok(())
    }
}

pub struct DeleteProject {
    pub id: i32,
}

impl Message for DeleteProject {
    type Result = Result<(), web::Error>;
}

impl Handler<DeleteProject> for DbExecutor {
    type Result = <DeleteProject as Message>::Result;

    fn handle(&mut self, msg: DeleteProject, _: &mut Self::Context) -> Self::Result {
        let conn = &self.0.get().unwrap();
        conn.execute(SQL_DELETE_PROJECT, &[&msg.id])
            .map_err(web::error::ErrorInternalServerError)?;
        Ok(())
    }
}

fn parse_array(s: &String) -> Vec<&str> {
    s.split("\r\n")
        .map(str::trim)
        .filter(|s| !s.is_empty())
        .collect()
}
