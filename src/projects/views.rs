use gettext_macros::i18n;
use maud::{html, Markup};

use crate::i18n::I18n;
use crate::users::User;
use crate::views::{br_list, comma_list, Date, head, header,
                   lang_dropdown, lang_icon, Markdown};

use super::models::{Project, ProjectWithChapters};

pub fn index(i18n: &I18n, user: Option<&User>, projects: Vec<Project>, search: Option<String>) -> Markup {
    html! {
        (head(i18n, &i18n!(i18n.catalog, "Projects")))
        (header(&i18n, user))
        h1 { (i18n!(i18n.catalog, "Project list")) }

        @if let Some(search) = search {
            h2 { (i18n!(i18n.catalog, "Searching for {}"; search)) }
        }
        form method="GET" action="/projects/" {
            input type="text" name="search" placeholder=(i18n!(i18n.catalog, "Search projects"));
            input type="submit" value=(i18n!(i18n.catalog, "Search"));
        }

        a href="/projects/new" { (i18n!(i18n.catalog, "New project")) }

        table class="wide projects info" {
        thead {
            tr {
                th {}
                th { (i18n!(i18n.catalog, "Name")) }
                th { (i18n!(i18n.catalog, "Authors")) }
            }
        }
        tbody {
            @for project in &projects {
                tr {
                    td {
                        (lang_icon(&project.language))
                    }
                    td {
                        a href={"/projects/"(project.id)} { (project.name) }
                    }
                    td {
                        (comma_list(&project.authors))
                    }
                }
            }
        }
        }
    }
}

pub fn new(i18n: &I18n, user: Option<&User>) -> Markup {
    html! {
        (head(i18n, &i18n!(i18n.catalog, "New project")))
        (header(&i18n, user))
        a href="/projects/" { (i18n!(i18n.catalog, "Back")) }
        h1 { (i18n!(i18n.catalog, "New project")) }
        (form(i18n, Project::default()))
    }
}

pub fn show(i18n: &I18n, user: Option<&User>, project: ProjectWithChapters) -> Markup {
    html! {
        (head(i18n, &project.info.name))
        (header(&i18n, user))
        table class="project info" {
        thead {
            tr {
                th colspan="2" {
                    @if project.info.has_cover {
                        img class="cover thumbnail"
                            src={"/projects/"(project.info.id)"/cover.jpg"}
                            alt=(i18n!(i18n.catalog, "Project cover"));
                        br;
                    }
                    (project.info.name)
                }
            }
        }
        tbody {
            tr {
                td { (i18n!(i18n.catalog, "Alt. names")) }
                td { (br_list(&project.info.alt_names)) }
            }
            tr {
                td { (i18n!(i18n.catalog, "Authors")) }
                td { (br_list(&project.info.authors)) }
            }
            tr {
                td { (i18n!(i18n.catalog, "Language")) }
                td { (lang_icon(&project.info.language)) }
            }
            tr {
                td colspan="2" {
                    a href={"/projects/"(project.info.id)"/edit"} {
                        button { (i18n!(i18n.catalog, "Edit")) }
                    }
                    form class="inline" method="POST" action={"/projects/"(project.info.id)"/delete"} {
                        input type="submit" value={(i18n!(i18n.catalog, "Delete"))};
                    }
                }
            }
        }
        }

        h1 { (project.info.name) }

        @if !project.info.has_locale_description() {
            em {
                (i18n!(i18n.catalog, "This project doesn't have a description in your language, showing it in English instead."))
            }
            article lang="en" {
                (Markdown(&project.info.description()))
            }
        } @else {
            article {
                (Markdown(&project.info.description()))
            }
        }

        h2 id="chapter-list" { (i18n!(i18n.catalog, "Chapter list")) }
        a href={"/projects/"(project.info.id)"/chapters/new"} {
            (i18n!(i18n.catalog, "New chapter"))
        }
        table class="wide chapters info" {
        thead {
            tr {
                th {}
                th { (i18n!(i18n.catalog, "Name")) }
                th { (i18n!(i18n.catalog, "Published")) }
            }
        }
        tbody {
            @for chapter in &project.chapters {
                tr {
                    td { (lang_icon(&chapter.language)) }
                    td {
                    a href={"/projects/"(project.info.id)"/chapters/"(chapter.id)} {
                        @if let Some(ref volume) = chapter.volume {
                            (i18n!(i18n.catalog, "Vol. {}, Ch. {}"; volume, &chapter.number))
                        } @else {
                            (i18n!(i18n.catalog, "Ch. {}"; &chapter.number))
                        }
                        @if let Some(ref name) = chapter.name {
                            " - " (name)
                        }
                    }
                    }
                    td {
                        @if let Some(pub_at) = chapter.published_at {
                            (Date(i18n, pub_at))
                        } @else {
                            em { (i18n!(i18n.catalog, "No")) }
                            " ("
                            a href={"/projects/"(project.info.id)"/chapters/"(chapter.id)"/edit#publish"} {
                                (i18n!(i18n.catalog, "Publish"))
                            }
                            ")"
                        }
                    }
                }
            }
        }
        }
    }
}

pub fn edit(i18n: &I18n, user: Option<&User>, project: Project) -> Markup {
    html! {
        (head(i18n, &i18n!(i18n.catalog, "Editing a project")))
        (header(&i18n, user))
        a href={"/projects/"(project.id)} {
            (i18n!(i18n.catalog, "Go back to project"))
        }
        h1 { (i18n!(i18n.catalog, "Editing a project")) }
        h2 class="subtitle" { (project.name) }
        (form(&i18n, project))
    }
}

fn form(i18n: &I18n, project: Project) -> Markup {
    html! {
        form class="details" method="POST" action={ "/projects/" {@if project.id > 0 { (project.id) }} } {
            label { (i18n!(i18n.catalog, "Name")) }
            input type="text" name="name" value=(project.name) required?;

            label { (i18n!(i18n.catalog, "Alternative names")) }
            em {
                (i18n!(i18n.catalog, "One name per line. Empty lines are ignored."))
            }
            textarea name="alt_names" required? {
                @for alt_name in &project.alt_names {
                    (alt_name) "\r\n"
                }
            }

            label { (i18n!(i18n.catalog, "Authors")) }
            em {
                (i18n!(i18n.catalog, "One author per line. Empty lines are ignored."))
            }
            textarea name="authors" required? {
                @for author in &project.authors {
                    (author) "\r\n"
                }
            }

            label { (i18n!(i18n.catalog, "Original language")) }
            select name="language" required? {
                (lang_dropdown(&i18n.catalog, &project.language))
            }

            label { (i18n!(i18n.catalog, "English description")) }
            em {
                a href="https://commonmark.org/help/" target="_blank" {
                    (i18n!(i18n.catalog, "You can use markdown in this field."))
                }
            }
            textarea name="description_en" required? {
                (project.description_en)
            }

            @if i18n.lang != "en" {
                label { (i18n!(i18n.catalog, "Description in your language")) }
                em {
                    (i18n!(i18n.catalog, "This field is optional."))
                    " "
                    a href="https://commonmark.org/help/" target="_blank" {
                        (i18n!(i18n.catalog, "You can use markdown in this field."))
                    }
                }
                textarea name="description" {
                    (project.description.unwrap_or_else(String::new))
                }
            }

            input type="submit" value=(i18n!(i18n.catalog, "Send"));
        }
    }
}
