#![feature(proc_macro_hygiene)]

use std::{env, io, process};
use std::sync::Arc;

use env_logger::fmt;
use gettext_macros::include_i18n;

use crate::config::Config;
use crate::db::DbExecutor;

mod app;
mod config;
mod db;
mod i18n;
mod projects;
mod users;
mod views;

fn prepare_key(key: String) -> Vec<u8> {
    let mut bytes = key.into_bytes();
    if bytes.len() < 32 {
        bytes.resize(32, 0);
    }
    bytes
}

fn log_format(buf: &mut fmt::Formatter, rec: &log::Record) -> io::Result<()> {
    use std::io::Write as _;

    let now = chrono::Utc::now().naive_local()
        .format("%Y-%m-%d %H:%M:%S%.6f").to_string();
    let mut style = buf.style();
    match rec.level() {
        log::Level::Error => {
            style.set_color(fmt::Color::Red).set_bold(true);
        }
        log::Level::Warn => {
            style.set_color(fmt::Color::Yellow);
        }
        _ => {}
    }
    writeln!(buf, "{} {:<5} {}", style.value(now),
             style.value(rec.level()), style.value(rec.args()))
}

fn print_help<T>() -> T {
    eprintln!("Usage: lumiere CONFIG_FILE");
    process::exit(1)
}

fn print_version() {
    println!("lumiere v{}", env!("CARGO_PKG_VERSION"));
    process::exit(1);
}

fn main() {
    if cfg!(debug_assertions) {
        env::set_var("RUST_LOG", "lumiere=debug,actix_web=debug");
        env::set_var("RUST_BACKTRACE", "1");
    } else {
        env::set_var("RUST_LOG", "lumiere=info,actix_web=warn");
    }
    env_logger::builder().format(log_format).init();

    let arg = env::args().nth(1);

    if let Some(ref arg) = arg {
        if &*arg == "-h" || &*arg == "--help" {
            print_help::<()>();
        } else if &*arg == "-v" || &*arg == "--version" {
            print_version();
        }
    }

    let Config {
        bind_to_address,
        secret_key,
        database,
    } = Config::read(arg);
    let db = DbExecutor::new(database);

    let sys = actix::System::new("lumiere");
    let db = actix::SyncArbiter::start(1, move || db.clone());

    let secret_key = prepare_key(secret_key);
    let translations = Arc::new(include_i18n!());

    log::info!("Listening on {}", bind_to_address);

    actix_web::server::new(move || {
        app::new(db.clone(), translations.clone(), &secret_key)
    }).bind(bind_to_address).unwrap().start();

    process::exit(sys.run())
}
