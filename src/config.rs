use std::error::Error;
use std::fs::read_to_string;
use std::io;
use std::path::Path;

use log::error;
use r2d2_postgres::postgres::params::{ConnectParams, Host, IntoConnectParams};
use serde::Deserialize;

#[derive(Deserialize)]
pub struct Config {
    pub bind_to_address: String,
    pub secret_key: String,
    pub database: DbConfig,
}

#[derive(Clone, Deserialize)]
pub struct DbConfig {
    pub host: String,
    pub port: u16,
    pub name: String,
    pub user: String,
    pub password: Option<String>,
}

impl Config {
    pub fn read<P>(path: Option<P>) -> Config
        where P: AsRef<Path>
    {
        if let Some(path) = path {
            Config::from_file(path)
        } else {
            log::info!("No configuration file given, reading from STDIN");
            Config::from_reader(io::stdin())
        }
    }

    pub fn from_file<P>(path: P) -> Config
        where P: AsRef<Path>
    {
        let contents = read_to_string(path).unwrap_or_else(fail);
        toml::from_str(&contents).unwrap_or_else(fail)
    }

    pub fn from_reader<R>(mut r: R) -> Config
        where R: io::Read
    {
        let mut contents = String::new();
        r.read_to_string(&mut contents).unwrap_or_else(fail);
        toml::from_str(&contents).unwrap_or_else(fail)
    }
}

impl IntoConnectParams for DbConfig {
    fn into_connect_params(self)
                           -> Result<ConnectParams, Box<Error + 'static + Send + Sync>>
    {
        Ok(ConnectParams::builder()
            .port(self.port)
            .user(&self.user, None)
            .database(&self.name)
            .build(Host::Tcp(self.host)))
    }
}

fn fail<T, E: Error>(e: E) -> T {
    error!("Failed to read the configuration file: {}", e);
    error!("More details: {:?}", e);
    std::process::exit(1)
}
