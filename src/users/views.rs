use gettext_macros::i18n;
use maud::{html, Markup};

use crate::i18n::I18n;
use crate::users::models::User;
use crate::views::{head, header};

pub fn profile(i18n: &I18n, user: User) -> Markup {
    html! {
        (head(i18n, &i18n!(i18n.catalog, "Login")))
        (header(&i18n, Some(&user)))
        "Logged in as " (user.username)
    }
}

pub fn login(i18n: &I18n) -> Markup {
    html! {
        (head(i18n, &i18n!(i18n.catalog, "Login")))
        (header(&i18n, None))
        h1 { (i18n!(i18n.catalog, "Login")) }

        form class="login" method="POST" action="/users/login" {
            label { (i18n!(i18n.catalog, "Username")) }
            input type="text" name="username" required={};
            br;
            label { (i18n!(i18n.catalog, "Password")) }
            input type="password" name="password" required={};
            br;
            input type="submit" value=(i18n!(i18n.catalog, "Send"));
        }
    }
}

pub fn register(i18n: &I18n) -> Markup {
    html! {
        (head(i18n, &i18n!(i18n.catalog, "Register")))
        (header(&i18n, None))
        h1 { (i18n!(i18n.catalog, "Register")) }

        form class="login" method="POST" action="/users/register" {
            label { (i18n!(i18n.catalog, "Username")) }
            input type="text"
                  name="username"
                  pattern="\\s*.{3,64}\\s*"
                  title=(i18n!(i18n.catalog, "Username must be between 3 and 64 characters long"))
                  required={};
            br;
            label { (i18n!(i18n.catalog, "Password")) }
            input type="password"
                  name="password"
                  pattern=".{8,}"
                  title=(i18n!(i18n.catalog, "Password must be at least 8 characters long"))
                  required={};
            br;
            label { (i18n!(i18n.catalog, "Confirm password")) }
            input type="password" name="confirm" required={};
            br;
            input type="submit" value=(i18n!(i18n.catalog, "Send"));
        }
    }
}
