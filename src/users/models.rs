use std::borrow::Cow;

use actix::{Handler, Message};
use actix_web as web;
use actix_web::middleware::identity::RequestIdentity;
use futures::Future;
use serde::Deserialize;

use crate::app;
use crate::db::DbExecutor;

const USERNAME_MIN_LENGTH: usize = 3;
const USERNAME_MAX_LENGTH: usize = 64;
const PASSWORD_MIN_LENGTH: usize = 8;
const HASH_COST: u32 = bcrypt::DEFAULT_COST;
const EMPTY_PASSWORD: &str = "$2y$12$OUnVKXyc4Qfjrf5fsYvUJ.ZIBP.Apjzqk68ASIQCDhjKbAiJoOdMO";

const SQL_FETCH_CREDENTIALDS: &str = "
SELECT id, username, password
FROM users
WHERE username = $1
";

const SQL_CREATE_USER: &str = "
INSERT INTO users (username, password)
VALUES ($1, $2)
RETURNING id
";

const SQL_FETCH_USER: &str = "
SELECT
    id,
    username
FROM users
WHERE id = $1
";

pub struct Credentials {
    pub id: i32,
    pub username: String,
    pub password: String,
}

impl Credentials {
    pub fn dummy() -> Credentials {
        Credentials {
            id: 0,
            username: String::new(),
            password: String::from(EMPTY_PASSWORD),
        }
    }

    pub fn is_dummy(&self) -> bool {
        self.id == 0
    }
}

pub struct User {
    pub id: i32,
    pub username: String,
}

impl web::FromRequest<app::AppState> for Option<User> {
    type Config = ();
    type Result = web::dev::AsyncResult<Option<User>, web::Error>;

    fn from_request(req: &app::HttpRequest, _: &Self::Config) -> Self::Result {
        if let Some(id) = req.identity() {
            let id = match id.parse::<i32>() {
                Ok(id) => id,
                Err(_) => {
                    log::warn!("CRITICAL ERROR: secret_key has been compromised!
Change its value as soon as possible.
Malicious cookie content (should be a number): {:?}", id);
                    return web::dev::AsyncResult::ok(None);
                }
            };
            let f = req.state().db
                .send(FetchUser { id })
                .from_err()
                .and_then(move |user| {
                    let user = user?;
                    if user.is_none() {
                        log::warn!("CRITICAL ERROR: secret_key has been compromised!
Change its value as soon as possible.
Someone tried to login with an user ID of {}, but this ID doesn't exist.", id);
                    }
                    Ok(user)
                });
            web::dev::AsyncResult::future(Box::new(f))
        } else {
            web::dev::AsyncResult::ok(None)
        }
    }
}

#[derive(Clone, Deserialize)]
pub struct Login<'a> {
    pub username: Cow<'a, str>,
    pub password: Cow<'a, str>,
}

impl<'a> Login<'a> {
    pub fn clean(&self) -> Login {
        Login {
            username: self.username.trim().into(),
            password: self.password.trim().into(),
        }
    }

    pub fn validate(self) -> Result<Login<'a>, web::Error> {
        if self.username.len() < USERNAME_MIN_LENGTH ||
            self.username.len() > USERNAME_MAX_LENGTH
        {
            return Err(web::error::ErrorBadRequest("Username not valid"));
        }
        if self.password.len() < PASSWORD_MIN_LENGTH {
            return Err(web::error::ErrorBadRequest("Password not valid"));
        }
        Ok(self)
    }
}

impl<'a> Message for Login<'a> {
    type Result = Result<String, web::Error>;
}

impl<'a> Handler<Login<'a>> for DbExecutor {
    type Result = <Registration<'a> as Message>::Result;

    fn handle(&mut self, msg: Login<'a>, _: &mut Self::Context) -> Self::Result {
        let Login { username, password } = msg.clean().validate()?;
        let conn = &self.0.get().unwrap();

        let creds = conn.query(SQL_FETCH_CREDENTIALDS, &[&username])
            .map_err(|_| {
                web::error::ErrorInternalServerError("Failed to fetch user")
            })?
            .iter()
            .map(|row| Credentials {
                id: row.get(0),
                username: row.get(1),
                password: row.get(2),
            })
            .next()
            .unwrap_or_else(Credentials::dummy);

        let has_valid_creds = bcrypt::verify(password.as_ref(), &creds.password)
            .map_err(|_| {
                web::error::ErrorInternalServerError("Failed to verify password")
            })?;

        if !creds.is_dummy() && has_valid_creds {
            Ok(id_to_token(creds.id))
        } else {
            Err(web::error::ErrorInternalServerError("Invalid username/password"))
        }
    }
}

#[derive(Clone, Deserialize)]
pub struct Registration<'a> {
    pub username: Cow<'a, str>,
    pub password: Cow<'a, str>,
    pub confirm: Cow<'a, str>,
}

impl<'a> Registration<'a> {
    pub fn clean(&self) -> Registration {
        Registration {
            username: self.username.trim().into(),
            password: self.password.trim().into(),
            confirm: self.confirm.trim().into(),
        }
    }

    pub fn validate(self) -> Result<Registration<'a>, web::Error> {
        if self.username.len() < USERNAME_MIN_LENGTH ||
            self.username.len() > USERNAME_MAX_LENGTH
        {
            return Err(web::error::ErrorBadRequest("Username not valid"));
        }
        if self.password.len() < PASSWORD_MIN_LENGTH {
            return Err(web::error::ErrorBadRequest("Password not valid"));
        }
        if self.password != self.confirm {
            return Err(web::error::ErrorBadRequest("Confirmation not valid"));
        }
        Ok(self)
    }
}

impl<'a> Message for Registration<'a> {
    type Result = Result<String, web::Error>;
}

impl<'a> Handler<Registration<'a>> for DbExecutor {
    type Result = <Registration<'a> as Message>::Result;

    fn handle(&mut self, msg: Registration<'a>, _: &mut Self::Context) -> Self::Result {
        let Registration { username, password, .. } = msg.clean().validate()?;
        let conn = &self.0.get().unwrap();

        let hash = bcrypt::hash(password.as_ref(), HASH_COST)
            .map_err(|_| {
                // Either an error from the random gen, NUL byte in password or
                // cost is out of bounds.
                web::error::ErrorInternalServerError("")
            })?;

        let id = conn.query(SQL_CREATE_USER, &[&username, &hash])
            .map_err(|e| {
                if is_username_taken(e) {
                    web::error::ErrorInternalServerError("Username taken")
                } else {
                    web::error::ErrorInternalServerError("Failed to insert user")
                }
            })?
            .get(0)
            .get(0);

        Ok(id_to_token(id))
    }
}

struct FetchUser {
    pub id: i32,
}

impl Message for FetchUser {
    type Result = Result<Option<User>, web::Error>;
}

impl Handler<FetchUser> for DbExecutor {
    type Result = <FetchUser as Message>::Result;

    fn handle(&mut self, msg: FetchUser, _: &mut Self::Context) -> Self::Result {
        let conn = &self.0.get().unwrap();
        let user = conn.query(SQL_FETCH_USER, &[&msg.id])
            .map_err(|_| {
                web::error::ErrorInternalServerError("Failed to fetch user")
            })?
            .iter()
            .map(|row| User {
                id: row.get(0),
                username: row.get(1),
            })
            .next();
        Ok(user)
    }
}

fn id_to_token(id: i32) -> String {
    format!("{}", id)
}

fn is_username_taken(e: r2d2_postgres::postgres::Error) -> bool {
    use r2d2_postgres::postgres::error::DbError;
    match e.as_db() {
        Some(DbError { constraint: Some(constraint), .. }) => {
            &constraint[..] == "users_username_key"
        }
        _ => false,
    }
}
