use actix_web as web;
use actix_web::http::{Method, NormalizePath};
use actix_web::middleware::identity::RequestIdentity;
use futures::Future;

use crate::app;
use crate::i18n::I18n;

use self::models::*;
pub use self::models::User;

mod models;
mod views;

pub fn routes(scope: app::Scope) -> app::Scope {
    scope
        .resource("/me", |r| {
            r.method(Method::GET).with(profile)
        })
        .resource("/login", |r| {
            r.method(Method::GET).with(login);
            r.method(Method::POST).with_async(login_auth);
        })
        .resource("/register", |r| {
            r.method(Method::GET).with(register);
            r.method(Method::POST).with_async(register_auth);
        })
        .resource("/logout", |r| {
            r.method(Method::POST).f(logout);
        })
        .default_resource(|r| {
            // Add .method(Method::GET) to only redirect GET requests.
            r.h(NormalizePath::default());
        })
}

fn profile((i18n, user): (I18n, Option<User>)) -> impl web::Responder {
    if let Some(user) = user {
        views::profile(&i18n, user)
    } else {
        views::login(&i18n)
    }
}

fn login((i18n, user): (I18n, Option<User>)) -> impl web::Responder {
    if user.is_some() {
        web::HttpResponse::SeeOther()
            .header("Location", "/users/me")
            .finish()
    } else {
        web::HttpResponse::Ok()
            .content_type("text/html; charset=utf-8")
            .body(views::login(&i18n).into_string())
    }
}

fn login_auth((item, state, req):
              (web::Form<Login<'static>>, app::State, app::HttpRequest))
              -> impl Future<Item=web::HttpResponse, Error=web::Error>
{
    let login = item.into_inner();
    state.db
        .send(login)
        .from_err()
        .and_then(move |id| {
            req.remember(id?);
            Ok(web::HttpResponse::SeeOther()
                .header("Location", "/users/me")
                .finish())
        })
}

fn register(i18n: I18n) -> impl web::Responder {
    views::register(&i18n)
}

fn register_auth((item, state, req):
                 (web::Form<Registration<'static>>, app::State, app::HttpRequest))
                 -> impl Future<Item=web::HttpResponse, Error=web::Error>
{
    let registration = item.into_inner();
    state.db
        .send(registration)
        .from_err()
        .and_then(move |id| {
            req.remember(id?);
            Ok(web::HttpResponse::SeeOther()
                .header("Location", "/users/me")
                .finish())
        })
}

fn logout(req: &app::HttpRequest) -> web::HttpResponse {
    req.forget();
    web::HttpResponse::SeeOther()
        .header("Location", "/")
        .finish()
}
