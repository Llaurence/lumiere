extern crate gettext_utils;

fn main() {
    if let Ok(_) = std::env::var("CREATE_DB") {
        // TODO use postgres to initialize the database.
    }

    gettext_utils::compile_i18n("lumiere", &["en", "fr"]);
}
