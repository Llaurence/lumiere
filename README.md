# lumiere

A Content Management System for Light/Web Novel translation teams.

## Setup the database

Install PostgreSQL.

Log in as `postgres`:

```console
$ sudo -i -u postgres
```

Create the database and its owner. Add `--pwprompt` to the first command to set
a password for the new user. 

```console
postgres$ createuser lumiere
postgres$ createdb lumiere --owner lumiere
```

Initialize the `hstore` extension (used to store project translations):

```console
postgres$ psql --username postgres --dbname lumiere
lumiere# CREATE EXTENSION hstore;
CREATE EXTENSION
lumiere# exit
postgres$ exit
```

Initialize the database with the correct tables:

```console
$ psql --username lumiere --file db/create.sql
CREATE TABLE
CREATE TABLE
...
```

Optionaly fill the database with sample data:

```console
$ psql --username lumiere --file db/fill.sql
DELETE 0
DELETE 0
INSERT 0 4
INSERT 0 12
```

## Setup lumiere

Fill `lumiere.toml` with the database name and its owner's name and password
from the section above.

## Build

```console
$ rustup override set nightly
$ cargo run
```
